import styled from "styled-components";

const StyledButtonWithProps = styled.button`
    background-color: ${props => props.bg};
    color: ${props => props.color};
    padding: 10px;
    border: none;
    border-radius: 5px;
    font-size: 1.25rem;
`;

export default StyledButtonWithProps;